# Introduction

Deployer is a tool to rsync files to a folder potentially on a remote server

# Usage

## Setup

You can start by calling the initialization on the script

    deployer --init

This will create you a `deploy` folder with some basic configuration

    deploy
    ├── config.cfg
    ├── post-deploy-script
    └── rsync-excludes

Some details about those files.

 - `config.cfg` See the related section
 - `post-deploy-script` Script that is executed after the deployment process. He receives `DRY_RUN`, `REMOTE` and `DEST_DIR` parameters
   You can remove this file if you have no post deployment action to do
 - `rsync-excludes` Regular rsync excludes file. See rsync configuration for detail

### Configuration

`rsync_exclude_file` (optional)

_default: './deploy/rsync-excludes'_

Path to the rsync-excludes file

`rsync_opts` (optional)

_default: '--no-perms --no-group --omit-dir-times'_

The options for rsync command. Options '-avc' are automatically added.

`src_dir` (mandatory)

The source directory.

`dest_dir` (mandatory)

The destination directory.

`remote` (optional)

_default: ''_

Remote on which to deploy.

## Execution

After you successfully configure the deployment, you can call the `deployer` commands.

    deployer

It will run a DRY-RUN deployment and show you the files that would be sent to the target.
Please refer to help of the commands to see how to really deploy your project.
