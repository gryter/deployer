#!/bin/bash

# Error and exit function
function error_exit
{
	echo "$1" 1>&2
	exit 1
}

# Usage informations
function display_usage
{
	echo -e "Usage: $(basename "$0") [OPTIONS]\n\n" \
			"Options:\n" \
			"\t\t--init\t\tInit a new deploy folder with basic config\n" \
			"-f\t\t--force\t\tForce the deployment (not a dry-run)\n" \
			"-r\t\t--reverse\tDownload online version\n" \
			"-h\t\t--help\t\tshow this help"
}

# Ask confirmation
function confirm()
{
	local _message=${1:-Are you sure (yes/no)? }
	while true; do
		read -p "$_message" reply
		case $reply in
			"yes" ) return 0; break;;
			"no" ) return 1; break;;
			[YyNn]* ) echo "Please completely type yes or no";;
			* ) echo "Please answer yes or no.";;
		esac
	done
}

# Init a new deployer
function init()
{
	# Get the real path to the current script
	SOURCE="${BASH_SOURCE[0]}"
	while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
		DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
		SOURCE="$(readlink "$SOURCE")"
		[[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
	done
	SCRIPTPATH="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

	CURRENT_PATH=$( pwd -P )
	DEPLOY_SRC=$SCRIPTPATH'/default-deploy'
	DEPLOY_DIR=$CURRENT_PATH'/deploy'

	# Create the deploy dir if it does not exists
	if [ ! -d "$DEPLOY_DIR" ]; then
		mkdir $DEPLOY_DIR
		# Check for mkdir error
		if [ $? -ne 0 ]; then exit 1; fi
	fi

	# Copy all files and folders from the default directory into the new one
	FILES="$DEPLOY_SRC/* $DEPLOY_SRC/.[!.]*"
	all_new=1
	for f in $FILES
	do
		filename=$(basename "$f")

		target="$DEPLOY_DIR/$filename"
		if [ -e $target ]; then
			all_new=0
		else
			cp -R $f $target
		fi
	done

	# Display confirmation message
	if [ $all_new -eq 1 ]; then
		echo "Initialized basic deploy folder in $DEPLOY_DIR"
	else
		echo "Reinitialized existing deploy folder in $DEPLOY_DIR"
	fi
}

# Execution variables and options
DRY_RUN=1
REVERSE_SIDE=0
while [ "$1" != "" ]; do
	case $1 in
		--init )			init
							exit
							;;
		-f | --force )		DRY_RUN=0
							;;
		-r | --reverse )	REVERSE_SIDE=1
							;;
		-h | --help )		display_usage
							exit
							;;
		* )					display_usage
							exit 1
	esac
	shift
done

# Load the configuration
CONFIG_FILE='./deploy/config.cfg'
if [ -f $CONFIG_FILE ];
then
	source $CONFIG_FILE
else
	error_exit "The deploy configuration was not found. Looked in 'deploy/config.cfg'"
fi

# Default and configuration variables
POST_DEPLOY_SCRIPT='./deploy/post-deploy-script'
RSYNC_EXCLUDE_FILE=${rsync_exclude_file:-'./deploy/rsync-excludes'}
RSYNC_OPTS=${rsync_opts:-'--no-perms --no-group --omit-dir-times'}
SRC_DIR=${src_dir:-''}
DEST_DIR=${dest_dir:-''}
REMOTE=${remote:-''}

# Check that the exlude file exists
if [ ! -f $RSYNC_EXCLUDE_FILE ];
then
	error_exit "The rsync-excludes file '$RSYNC_EXCLUDE_FILE' was not found."
fi

function deploy()
{
	local _src_dir=$1
	local _dest_dir=$2
	local _remote=$3

	# We at least need a source and destination folder
	if [ "$_src_dir" = "" ] || [ "$_dest_dir" = "" ]; then
		error_exit "You MUST specify source and destination folders in the configuration! Aborting."
	fi

	# Check if we deploy to a remote server
	# Set the correct rsync destination and save whether to fix online permissions
	if [ ! -z "$_remote" -a "$_remote" != " " ]; then
		local _rsync_dest="$_remote:$_dest_dir"
		local _should_fix_perm=1
	else
		local _rsync_dest="$_dest_dir"
		local _should_fix_perm=0
	fi

	# Inverse side ?
	if [ $REVERSE_SIDE -eq 0 ]; then
		local _rsync_src=$_src_dir
	else
		local _rsync_src=$_rsync_dest
		_rsync_dest=$_src_dir
		# Do not fix permissions in inverse side mode
		_should_fix_perm=0
	fi

	# Build rsync options according to config
	local _rsync_tmp_opts='-avzc'
	local _confirmed=0
	if [ $DRY_RUN -ne 0 ]; then
		_rsync_tmp_opts=$_rsync_tmp_opts'n'
		_confirmed=1
	else
		confirm "Are you sure you want to deploy to \"$_rsync_dest\" ? (yes/no): "

		if [ $? -eq 0 ]; then
			_confirmed=1
		else
			echo "Deployment aborted !"
			exit
		fi
	fi

	RSYNC_OPTS="$_rsync_tmp_opts $RSYNC_OPTS"

	if [ $_confirmed -eq 1 ];
	then
		# Display a reassuring message to know that we are in DRY-RUN or REAL deployment
		echo ""
		if [ $DRY_RUN -ne 0 ]; then
			echo "Launch DRY-RUN deployment..."
		else
			echo "Launch REAL deployment..."
		fi
		echo ""

		# Execute the rsync
		rsync \
			$RSYNC_OPTS \
			--exclude-from="$RSYNC_EXCLUDE_FILE" \
			"$_rsync_src" \
			"$_rsync_dest"

		# Check for Rsync error
		if [ $? -ne 0 ]; then
			echo "Rsync error ($?)! Aborting." 1>&2
			exit 1
		fi

		# Fix online permissions
		if [ $DRY_RUN -eq 0 ] && [ $_should_fix_perm -eq 1 ]; then
			echo ""
			echo "Fixing remote permissions..."
			ssh $_remote 'chmod -R go-w "'$_dest_dir'"'
		fi

		# Execute the post deployment script
		if [ -f "$POST_DEPLOY_SCRIPT" ];
		then
			echo ""
			echo "Execute post deployment script..."
			sh "$POST_DEPLOY_SCRIPT" "$DRY_RUN" "$_remote" "$_dest_dir"
		fi
	fi
}

deploy $SRC_DIR $DEST_DIR $REMOTE
